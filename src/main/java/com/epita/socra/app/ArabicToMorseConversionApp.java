package com.epita.socra.app;

import com.epita.socra.app.tools.ConsoleAdapter;
import com.epita.socra.app.tools.IOAdapter;

import java.util.HashMap;

public class ArabicToMorseConversionApp {

    private IOAdapter adapter;

    private ArabicToMorseConversionApp() {
        this(new ConsoleAdapter());
    }

    public ArabicToMorseConversionApp(IOAdapter adapter) {
        this.adapter = adapter;
    }

    public static void main(String[] args) {
        ArabicToMorseConversionApp application = new ArabicToMorseConversionApp();
        application.run();
    }

    public void run() {
        while (IntToMorseCode().length() != 0) {
            continue;
        }
    }

    /**
     * Convert arabic number from inputStream into morseCode, and displays it.
     */
    public String IntToMorseCode(){

        HashMap<Character, String> IntToMorse = new HashMap<Character, String>();
        IntToMorse.put('0', "_ _ _ _ _");
        IntToMorse.put('1', ". _ _ _ _");
        IntToMorse.put('2', ". . _ _ _");
        IntToMorse.put('3', ". . . _ _");
        IntToMorse.put('4', ". . . . _");
        IntToMorse.put('5', ". . . . .");
        IntToMorse.put('6', "_ . . . .");
        IntToMorse.put('7', "_ _ . . .");
        IntToMorse.put('8', "_ _ _ . .");
        IntToMorse.put('9', "_ _ _ _ .");

        String arabicNumber = adapter.read();

        String morseCode = "";

        for (int indexInput = 0; indexInput < arabicNumber.length(); indexInput++) {
            assert (Character.isDigit(arabicNumber.charAt(indexInput)));
            morseCode += IntToMorse.get(arabicNumber.charAt(indexInput)) + " ";
        }

        adapter.write(morseCode);

        return morseCode;
    }
}
