package com.epita.socra.app;

import com.epita.socra.app.tools.*;

import java.util.HashMap;

public final class MorseToArabicConversionApp {

    private IOAdapter adapter;

    private MorseToArabicConversionApp() {
        this(new ConsoleAdapter());
    }

    public MorseToArabicConversionApp(IOAdapter adapter) {
        this.adapter = adapter;
    }

    public static void main(String[] args) {
        MorseToArabicConversionApp application = new MorseToArabicConversionApp();
        application.run();
    }

    public void run() {
        while (MorseCodeToArabic().length() != 0) {
            continue;
        }
    }

    /**
     * Convert morse code from inputStream into arabic number, and displays it.
     */
    public String MorseCodeToArabic(){

        HashMap<String, Character> MorseToArabic = new HashMap<String, Character>();
        MorseToArabic.put("_ _ _ _ _", '0');
        MorseToArabic.put(". _ _ _ _", '1');
        MorseToArabic.put(". . _ _ _", '2');
        MorseToArabic.put(". . . _ _", '3');
        MorseToArabic.put(". . . . _", '4');
        MorseToArabic.put(". . . . .", '5');
        MorseToArabic.put("_ . . . .", '6');
        MorseToArabic.put("_ _ . . .", '7');
        MorseToArabic.put("_ _ _ . .", '8');
        MorseToArabic.put("_ _ _ _ .", '9');

        String morseCode = adapter.read();

        String arabicNumber = "";

        for (int indexInput = 0; indexInput < morseCode.length(); indexInput += 10) {
            arabicNumber += MorseToArabic.get(morseCode.substring(indexInput, indexInput + 9));
        }

        adapter.write(arabicNumber);

        return arabicNumber;
    }
}
