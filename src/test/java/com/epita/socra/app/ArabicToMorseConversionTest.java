package com.epita.socra.app;

import org.junit.Test;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.epita.socra.app.tools.IOAdapter;

/**
 * Unit test for simple App.
 */
public class ArabicToMorseConversionTest {

    /**
     * Morse conversion test
     */
    @Test
    public void givenInput0_expect_5underscores() {
        morseCodeConversionTest("0", "_ _ _ _ _ ");
    }

    @Test
    public void givenInput1_expect_1dot_4underscores() {
        morseCodeConversionTest("1", ". _ _ _ _ ");
    }

    @Test
    public void givenInput2_expect_2dot_3underscores() {
        morseCodeConversionTest("2", ". . _ _ _ ");
    }

    @Test
    public void givenInput3_expect_3dot_2underscores() {
        morseCodeConversionTest("3", ". . . _ _ ");
    }

    @Test
    public void givenInput4_expect_4dot_1underscores() {
        morseCodeConversionTest("4", ". . . . _ ");
    }

    @Test
    public void givenInput5_expect_5dot() {
        morseCodeConversionTest("5", ". . . . . ");
    }

    @Test
    public void givenInput6_expect_1underscore_4dot() {
        morseCodeConversionTest("6", "_ . . . . ");
    }

    @Test
    public void givenInput7_expect_2underscores_3dot() {
        morseCodeConversionTest("7", "_ _ . . . ");
    }

    @Test
    public void givenInput8_expect_3underscores_2dot() {
        morseCodeConversionTest("8", "_ _ _ . . ");
    }

    @Test
    public void givenInput9_expect_4underscores_1dot() {
        morseCodeConversionTest("9", "_ _ _ _ . ");
    }

    @Test
    public void givenInput10_expect_1underscores_4dot() {
        morseCodeConversionTest("10", ". _ _ _ _ _ _ _ _ _ ");
    }

    @Test
    public void givenInput83_expect_3underscores_5dot_2underscores() {
        morseCodeConversionTest("83", "_ _ _ . . . . . _ _ ");
    }

    @Test
    public void givenInput99_expect_4underscores_1dot_4underscores_1dot_4underscores() {
        morseCodeConversionTest("99", "_ _ _ _ . _ _ _ _ . ");
    }

    @Test
    public void givenInput1903_expect_1dot_8underscores_1dot_5underscores_3dot_2underscores() {
        morseCodeConversionTest("1903", ". _ _ _ _ _ _ _ _ . _ _ _ _ _ . . . _ _ ");
    }

    @Test
    public void givenEmptyInput_expect_EmptyString() {
        morseCodeConversionTest("", "");
    }

    public void morseCodeConversionTest(String input, String output) {
        IOAdapter mock = mock(IOAdapter.class);
        ArabicToMorseConversionApp app = new ArabicToMorseConversionApp(mock);

        when(mock.read()).thenReturn(input);
        app.IntToMorseCode();
        verify(mock).write(argThat(message -> message.contains(output)));
    }
}
