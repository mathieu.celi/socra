package com.epita.socra.app;

import com.epita.socra.app.tools.IOAdapter;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

/**
 * Unit test for simple App.
 */
public class MorseToArabicConversionTest {

    /**
     * Morse conversion test
     */
    @Test
    public void givenInput5underscores_expect0() {
        ArabicToMorseCodeConversionTest("_ _ _ _ _", "0");
    }

    @Test
    public void givenInput1dot_4underscores_expect1() {
        ArabicToMorseCodeConversionTest(". _ _ _ _", "1");
    }

    @Test
    public void givenInput2dot_3underscores_expect2() {
        ArabicToMorseCodeConversionTest(". . _ _ _", "2");
    }

    @Test
    public void givenInput3dot_2underscores_expect3() {
        ArabicToMorseCodeConversionTest( ". . . _ _ ", "3");
    }

    @Test
    public void givenInput4dot_1underscores_expect4() {
        ArabicToMorseCodeConversionTest(". . . . _", "4");
    }

    @Test
    public void givenInput5dot_expect5() {
        ArabicToMorseCodeConversionTest(". . . . .", "5");
    }

    @Test
    public void givenInput1underscore_4dot_expect6() {
        ArabicToMorseCodeConversionTest("_ . . . .", "6");
    }

    @Test
    public void givenInput2underscores_3dot_expect7() {
        ArabicToMorseCodeConversionTest("_ _ . . .", "7");
    }

    @Test
    public void givenInput3underscores_2dot_expect8() {
        ArabicToMorseCodeConversionTest("_ _ _ . .", "8");
    }

    @Test
    public void givenInput4underscores_1dot_expect9() {
        ArabicToMorseCodeConversionTest("_ _ _ _ .", "9");
    }

    @Test
    public void givenInput1underscores_4dot_expect10() {
        ArabicToMorseCodeConversionTest( ". _ _ _ _ _ _ _ _ _", "10");
    }

    @Test
    public void givenInput3underscores_5dot_2underscores_expect83() {
        ArabicToMorseCodeConversionTest("_ _ _ . . . . . _ _", "83");
    }

    @Test
    public void givenInput4underscores_1dot_4underscores_1dot_4underscores_expect99() {
        ArabicToMorseCodeConversionTest("_ _ _ _ . _ _ _ _ .", "99");
    }

    @Test
    public void givenInput1dot_8underscores_1dot_5underscores_3dot_2underscores_expect1903() {
        ArabicToMorseCodeConversionTest(". _ _ _ _ _ _ _ _ . _ _ _ _ _ . . . _ _", "1903");
    }

    @Test
    public void givenEmptyInput_expect_EmptyString() {
        ArabicToMorseCodeConversionTest("", "");
    }

    public void ArabicToMorseCodeConversionTest(String input, String output) {
        IOAdapter mock = mock(IOAdapter.class);
        MorseToArabicConversionApp app = new MorseToArabicConversionApp(mock);

        when(mock.read()).thenReturn(input);
        app.MorseCodeToArabic();
        verify(mock).write(argThat(message -> message.contains(output)));
    }
}
